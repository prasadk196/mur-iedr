import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable, of } from 'rxjs';
 
@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
 
  public loading$ = new BehaviorSubject<boolean>(false);

  setLoading(flag = false) {
    this.loading$.next(flag);
  }

  getLoading(): Observable<boolean> {
    return this.loading$;
  }

}