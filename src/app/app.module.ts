import { CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuModule } from 'primeng/menu';
import { RippleModule } from 'primeng/ripple';
import { MenubarModule } from 'primeng/menubar';
import { DropdownModule } from 'primeng/dropdown';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AppRoutingModule } from './app-routing.module';
import { DocumentRetrievalComponent } from './home/retrieval/document-retrieval/document-retrieval.component';
import { CommentboxComponent } from './home/retrieval/case-comments/commentbox/commentbox.component';
import { ChildboxComponent } from './home/retrieval/case-comments/childbox/childbox.component';
import { CommentsComponent } from './home/retrieval/case-comments/comments/comments.component';
import { CaseCommentsComponent } from './home/retrieval/case-comments/case-comments.component';
import { EdrReportsComponent } from './home/reporting/edr-reports/edr.reports.component';
import { DatacontainerDirective } from './home/retrieval/case-comments/comments/comments.component';
import { ViewDocumentsComponent } from './home/retrieval/document-retrieval/view-documents/view-documents.component';
import { CommonReports } from './home/reporting/edr-reports/common-reports/common-reports.component';
import { ReportsDetailsComponent } from './home/reporting/edr-reports/reports-details/reports-details.component';
import { PathComponent } from './path/path.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AccordionModule } from 'primeng/accordion';
import { ToastModule } from 'primeng/toast';
import { DocumentEditorModule } from '@txtextcontrol/tx-ng-document-editor';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';
import { TableModule } from 'primeng/table';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { AuthService } from './auth.service';
import { SubHeaderComponent } from './sub-header/sub-header.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { DistrictSummaryReportComponent } from './home/reporting/edr-reports/district-summary-report/district-summary-report.component';
import { UserActivityDetailReportComponent } from './home/reporting/edr-reports/user-activity-detail-report/user-activity-detail-report.component';
import { UserActivityEventSummaryReportComponent } from './home/reporting/edr-reports/user-activity-event-summary-report/user-activity-event-summary-report.component';
import { UserActivitySummaryReportComponent } from './home/reporting/edr-reports/user-activity-summary-report/user-activity-summary-report.component';
import { UserErrorAndExceptionComponent } from './home/reporting/edr-reports/user-error-and-exception/user-error-and-exception.component';
import { ManageSensitiveCaseAccess } from './home/management/manage-sensitive-case-access/manage-sensitive-case-access.component';
import { ManageAuditAccess } from './home/management/manage-audit-access/manage-audit-access.component';
import { ManageAuditCases } from './home/management/manage-audit-access/manage-audit-cases/manage-audit-cases.component';
import { NewAuditor } from './home/management/manage-audit-access/new-auditor/new-auditor.component';
import { AddRemoveUsers } from './home/management/manage-sensitive-case-access/add-remove-users/add-remove-users.component'
import { BatchRetrievalComponent } from './oss-main-menu/batch-retrieval/batch-retrieval.component';
import {CheckboxModule} from 'primeng/checkbox';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {OSSReportingComponent} from './oss-main-menu/reporting/reporting.component';
import {OSSDistrictSummaryComponent} from './oss-main-menu/reporting/district-summary/district-summary.component';
import { OSSScanningComponent } from './oss-main-menu/scanning/scanning.component';
import { OSSMainMenuComponent } from './oss-main-menu/oss-main-menu.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { ModalModule } from 'ngx-bootstrap/modal';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import { OSSIndexingEventetailComponent } from "./oss-main-menu/reporting/indexing-event-detail/indexing-event-detail.component";
import { ViewFileComponent } from './common/view-file.component';
import { MessageService } from 'primeng/api';
import { OSSScanningEventetailComponent } from './oss-main-menu/reporting/scanning-event-detail/scanning-event-detail.component';
import { OSSUserDetailSummaryComponent } from './oss-main-menu/reporting/user-detail-summary/user-detail-summary.component';

import {
  OKTA_CONFIG,
  OktaAuthModule
} from '@okta/okta-angular';
import { OktaAuth } from '@okta/okta-auth-js';
import config  from './config/okta-config';

const oktaAuth = new OktaAuth(config.oidc);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DocumentRetrievalComponent,ReportsDetailsComponent,
    HeaderComponent,EdrReportsComponent,CommonReports,
    CaseCommentsComponent,
    ViewDocumentsComponent,
    CommentboxComponent,
    CommentsComponent,
    ChildboxComponent,PathComponent,LoginComponent,
    DatacontainerDirective,
    SubHeaderComponent,DropdownComponent,
    DistrictSummaryReportComponent,
    UserActivityDetailReportComponent,
    UserActivityEventSummaryReportComponent,
    UserActivitySummaryReportComponent,
    UserErrorAndExceptionComponent,
    ManageSensitiveCaseAccess,
    ManageAuditAccess,
    ViewFileComponent,
    ManageAuditCases,NewAuditor,AddRemoveUsers,BatchRetrievalComponent,
    OSSMainMenuComponent, OSSReportingComponent,OSSDistrictSummaryComponent,OSSScanningComponent,
    OSSIndexingEventetailComponent,OSSScanningEventetailComponent,OSSUserDetailSummaryComponent
  ],
  imports: [
    BrowserModule,HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,ReactiveFormsModule,
    MenuModule,
    MenubarModule,CheckboxModule,InputTextareaModule,
    RippleModule,
    ButtonModule, 
    DropdownModule,
    RadioButtonModule,
    InputTextModule,AccordionModule,ToastModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),DocumentEditorModule,
    CKEditorModule,
    PdfJsViewerModule,
    CKEditorModule,
    TableModule,
    NgxExtendedPdfViewerModule,
    OktaAuthModule,
    ProgressSpinnerModule,
    ModalModule.forRoot()
  ],
  entryComponents: [ChildboxComponent, ViewFileComponent],
  providers: [
    AuthService,
    MessageService,
    { 
      provide: OKTA_CONFIG, 
      useValue: {oktaAuth}
    },
  ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
function Router(Router: any): any {
  throw new Error('Function not implemented.');
}

