import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { DocumentRetrievalComponent } from "./home/retrieval/document-retrieval/document-retrieval.component";
import { ViewDocumentsComponent } from "./home/retrieval/document-retrieval/view-documents/view-documents.component";
import { HomeComponent } from "./home/home.component";
import { CaseCommentsComponent } from "./home/retrieval/case-comments/case-comments.component";
import { EdrReportsComponent } from "./home/reporting/edr-reports/edr.reports.component";
import { ReportsDetailsComponent } from './home/reporting/edr-reports/reports-details/reports-details.component';
import { LoginComponent } from "./login/login.component";
import { DistrictSummaryReportComponent } from './home/reporting/edr-reports/district-summary-report/district-summary-report.component';
import { UserActivityDetailReportComponent } from './home/reporting/edr-reports/user-activity-detail-report/user-activity-detail-report.component';
import { UserActivityEventSummaryReportComponent } from './home/reporting/edr-reports/user-activity-event-summary-report/user-activity-event-summary-report.component';
import { UserActivitySummaryReportComponent } from './home/reporting/edr-reports/user-activity-summary-report/user-activity-summary-report.component';
import { UserErrorAndExceptionComponent } from './home/reporting/edr-reports/user-error-and-exception/user-error-and-exception.component';
import { ManageSensitiveCaseAccess } from './home/management/manage-sensitive-case-access/manage-sensitive-case-access.component';
import { ManageAuditAccess } from './home/management/manage-audit-access/manage-audit-access.component';
import { ManageAuditCases } from './home/management/manage-audit-access/manage-audit-cases/manage-audit-cases.component';
import { AddRemoveUsers } from './home/management/manage-sensitive-case-access/add-remove-users/add-remove-users.component'
import { NewAuditor } from './home/management/manage-audit-access/new-auditor/new-auditor.component';
import {OSSReportingComponent} from './oss-main-menu/reporting/reporting.component';
import {OSSDistrictSummaryComponent} from './oss-main-menu/reporting/district-summary/district-summary.component';
import { OSSScanningComponent } from './oss-main-menu/scanning/scanning.component';
import { OSSMainMenuComponent } from "./oss-main-menu/oss-main-menu.component";
import { BatchRetrievalComponent } from "./oss-main-menu/batch-retrieval/batch-retrieval.component";
import { CaseDetailResolver } from "./app.resolver";
import { OktaAuthGuard, OktaCallbackComponent } from "@okta/okta-angular";
import { OSSIndexingEventetailComponent } from "./oss-main-menu/reporting/indexing-event-detail/indexing-event-detail.component";
import { OSSScanningEventetailComponent } from "./oss-main-menu/reporting/scanning-event-detail/scanning-event-detail.component";
import { OSSUserDetailSummaryComponent } from "./oss-main-menu/reporting/user-detail-summary/user-detail-summary.component";

const routes: Routes = [
    { path: '', redirectTo:'/login', pathMatch: 'full'}, 
    { path: 'login', component: LoginComponent },
    { path: 'login/callback', component: OktaCallbackComponent },
    { path: 'home', component: HomeComponent, canActivate: [OktaAuthGuard] },
    {   
        path: 'home/document-retrieval', 
        component: DocumentRetrievalComponent,
        canActivate: [OktaAuthGuard]
    },
    {    
        path: 'home/document-retrieval/view-documents',
        component: ViewDocumentsComponent,
        canActivate: [OktaAuthGuard],
        resolve: { caseDetail: CaseDetailResolver } 
    },
    { path: 'home/case-comments', component: CaseCommentsComponent,canActivate: [OktaAuthGuard] },
    { path: 'home/edr-reports', component: EdrReportsComponent,canActivate: [OktaAuthGuard] },
    { path: 'home/edr-reports/:id/reports-details', component: ReportsDetailsComponent,canActivate: [OktaAuthGuard] },
    { path: 'home/edr-reports/district-summary-report', component: DistrictSummaryReportComponent,canActivate: [OktaAuthGuard] },
    { path: 'home/edr-reports/user-activity-detail-report', component: UserActivityDetailReportComponent,canActivate: [OktaAuthGuard] },
    { path: 'home/edr-reports/user-activity-event-summary-report', component: UserActivityEventSummaryReportComponent,canActivate: [OktaAuthGuard] },
    { path: 'home/edr-reports/user-activity-summary-report', component: UserActivitySummaryReportComponent,canActivate: [OktaAuthGuard] },
    { path: 'home/edr-reports/user-error-and-exception-report', component: UserErrorAndExceptionComponent,canActivate: [OktaAuthGuard] },
    { path: 'home/manage-sensitive-case-access', component: ManageSensitiveCaseAccess,canActivate: [OktaAuthGuard] },
    { path: 'home/manage-sensitive-case-access/add-remove-users', component: AddRemoveUsers,canActivate: [OktaAuthGuard] },
    { path: 'home/manage-audit-access', component: ManageAuditAccess,canActivate: [OktaAuthGuard] },
    { path: 'home/manage-audit-access/manage-audit-cases', component: ManageAuditCases,canActivate: [OktaAuthGuard] },
    { path: 'home/manage-audit-access/new-auditor', component: NewAuditor,canActivate: [OktaAuthGuard] },
    { path: 'oss-main-menu', component: OSSMainMenuComponent,canActivate: [OktaAuthGuard] },
    { path: 'oss-main-menu/batch-retrieval', component: BatchRetrievalComponent, canActivate: [OktaAuthGuard] },
    { path: 'oss-main-menu/reporting', component: OSSReportingComponent,canActivate: [OktaAuthGuard] },
    { path: 'oss-main-menu/reporting/district-summary', component: OSSDistrictSummaryComponent,canActivate: [OktaAuthGuard] },
    { path: 'oss-main-menu/reporting/indexing-event-detail', component: OSSIndexingEventetailComponent,canActivate: [OktaAuthGuard] },
    { path: 'oss-main-menu/reporting/scanning-event-detail', component: OSSScanningEventetailComponent,canActivate: [OktaAuthGuard] },
    { path: 'oss-main-menu/reporting/user-detail-summary', component: OSSUserDetailSummaryComponent,canActivate: [OktaAuthGuard] },
    { path: 'oss-main-menu/scanning', component: OSSScanningComponent,canActivate: [OktaAuthGuard] },
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [RouterModule]
})

export class AppRoutingModule { }