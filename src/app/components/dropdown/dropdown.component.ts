import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
})

export class DropdownComponent implements OnInit {

  @Input() items: any;
  @Input() selectedItem: any;
  @Output() getSelectedValue = new EventEmitter<string>();
  
  
  constructor() { }

  ngOnInit() {

  }

  getSelectedItem(event){
    this.getSelectedValue.emit(event.value);

  }

}
