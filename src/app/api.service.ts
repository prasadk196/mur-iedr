import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError, Observable, of, throwError } from 'rxjs';
import { SpinnerService } from './services/spinner.service';
 
@Injectable({
  providedIn: 'root'
})
export class ApiService {
 
  constructor(
    private httpClient: HttpClient,
    private spinnerService: SpinnerService
  ) { }

  public getDocumentsById(id?: any){
    return this.httpClient.get(`${environment.API_PATH}getDocument?documentId=${id}`);
  }

  getCaseDetails(req?: any){
    if(req.caseNumber) {
      return this.getCaseDetailByCaseNumber(req);
    } else if(req.CIN || req.SSN) {
      return this.getCaseRetrievalByClientInfo(req);
    } else if (req.appNumber) {
      return this.getCaseRetrievalByApplication(req);
    }  else if (req.orphans) {
      return this.getCaseRetrievalByOrphans(req);
    }
  }

  public getCaseDetailByCaseNumber(req?: any){
    this.spinnerService.setLoading(true);
    return this.httpClient.post(`${environment.API_PATH}caseRetrieval`, req)
    .pipe(catchError(
      (err) => {
        console.log(err);
        this.spinnerService.setLoading(false);
        return throwError(err)
    }));
  }

  public scannedFileUpload(req?: any){
    this.spinnerService.setLoading(true);
    return this.httpClient.post(`${environment.API_PATH}caseRetrieval`, req)
    .pipe(catchError(
      (err) => {
        console.log(err);
        this.spinnerService.setLoading(false);
        return throwError(err)
    }));
  }

  public getCaseRetrievalByClientInfo(req?: any){
    this.spinnerService.setLoading(true);
    return this.httpClient.post(`${environment.API_PATH}caseRetrievalByClientInfo`, req)
    .pipe(catchError(
      (err) => {
        console.log(err);
        this.spinnerService.setLoading(false);
        return throwError(err)
    }));
  }

  public getCaseRetrievalByApplication(req?: any){
    this.spinnerService.setLoading(true);
    return this.httpClient.post(`${environment.API_PATH}caseRetrievalByApplication`, req)
    .pipe(catchError(
      (err) => {
        console.log(err);
        this.spinnerService.setLoading(false);
        return throwError(err)
    }));
  }

  public getCaseRetrievalByOrphans(req?: any){
    this.spinnerService.setLoading(true);
    return this.httpClient.post(`${environment.API_PATH}caseRetrievalByOrphan`, req)
    .pipe(catchError(
      (err) => {
        console.log(err);
        this.spinnerService.setLoading(false);
        return throwError(err)
    }));
  }

}