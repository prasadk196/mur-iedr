import { Injectable } from '@angular/core';

@Injectable({
  providedIn : 'root'
})
export class AuthService {

  openOSSAPP(urlString: any = '') {
    const url = window.location.origin + window.location.pathname.substring(0, window.location.pathname.indexOf("/",2)) + 'oss-main-menu' + urlString;
    window.open(url, 'newwindow', 'toolbar=0,location=0,menubar=0');
  }
  
}
