export default {
  oidc : {
    // issuer: 'https://login-qa.ny.gov',
    // clientId: '0oaenk9feeaA7ymiZ297',
    issuer: 'https://dev-33813050.okta.com/oauth2/default',
    clientId: '0oa6c5qhsaIQoE0TO5d7',
    redirectUri: window.location.origin + window.location.pathname.substring(0, window.location.pathname.indexOf("/",2)) + '/login/callback',
      // redirectUri: 'http://localhost:4200/my-app/login/callback',
    scopes: ['openid','profile','email']
  }
}