import { Component, Inject, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SpinnerService } from './services/spinner.service';

import { OktaAuth } from '@okta/okta-auth-js';
import { OktaAuthStateService, OKTA_AUTH } from '@okta/okta-angular';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'mur-iedr';
  user: string = '';
  public loading$: Observable<boolean>;

  constructor(
    private spinnerService: SpinnerService
  ) {
    this.loading$ = this.spinnerService.getLoading();
  }  
}
