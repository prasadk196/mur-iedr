import { Component, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'view-file',
  templateUrl: './view-file.component.html',
})
export class ViewFileComponent {
  title = 'mur-iedr';
  pdfSrc = '';
  imgSrc = '';
  public event: EventEmitter<any> = new EventEmitter();

  constructor(
    private modalService: BsModalService
  ) {}

  hide() {
    this.modalService.hide();
  }

  submit() {
    this.triggerEvent(true);
    this.modalService.hide();
  }

  triggerEvent(item: any) {
    this.event.emit({ data: item , res:200 });
  }

}
