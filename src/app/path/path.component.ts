import { Component, OnInit } from "@angular/core";
import { Location } from '@angular/common';

@Component({
    selector: 'app-path',
    templateUrl: './path.component.html',
    // styleUrls: ['./path.component.scss'],
})

export class PathComponent implements OnInit {
   
    path;
    constructor(private location: Location) { 
        this.path = location.path().split('/');
    }
    ngOnInit() {
        
    }
    getpath(item){
      var data = '';
      if(item < this.path.length - 1){
        for(var i = 1; i <= item; i++){
            data += '/'+ this.path[i]
        }
        return data;
      }
    }

    convertPathText(text){
        return text.replaceAll("-", " ");
    }

}
