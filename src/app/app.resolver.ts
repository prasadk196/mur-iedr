import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { MessageService } from "primeng/api";
import { Observable, of } from "rxjs";
import { ApiService } from "./api.service";
import { SpinnerService } from "./services/spinner.service";

@Injectable({
  providedIn: 'root'
})
export class CaseDetailResolver implements Resolve<Observable<string>> {

  constructor(
    private apiService: ApiService,
    private spinnerService: SpinnerService
  ) {}

  resolve(): any {
    let documentSearchCriteria: any = localStorage.getItem('document-search-criteria'); 
    if(documentSearchCriteria) {
      documentSearchCriteria = JSON.parse(documentSearchCriteria);
      const req = {
        "userId": "SZT04",
        "district":"01",
        "machineId":"WMXL11320FC",
        "countyCode": (documentSearchCriteria.district && documentSearchCriteria.district.code ) ? documentSearchCriteria.district.code.toUpperCase() : null,
        "CIN": (documentSearchCriteria.clientInfo && documentSearchCriteria.clientInfo.cin ) ? documentSearchCriteria.clientInfo?.cin.toUpperCase() : null,
        "SSN": (documentSearchCriteria.clientInfo && documentSearchCriteria.clientInfo.ssn ) ? documentSearchCriteria.clientInfo?.ssn.toUpperCase() : null,
        "caseNumber": (documentSearchCriteria.caseInfo && documentSearchCriteria.caseInfo?.caseNumber ) ? documentSearchCriteria.caseInfo?.caseNumber.toUpperCase() : null,
        "appNumber": (documentSearchCriteria.applicationInfo && documentSearchCriteria.applicationInfo.application ) ? documentSearchCriteria.applicationInfo?.application: null,
        "orphans": (documentSearchCriteria.orphans && documentSearchCriteria.orphans.value ) ? documentSearchCriteria?.orphans.value: null
      }
      return this.apiService.getCaseDetails(req);
    }
  }
}