import { Component } from "@angular/core";
import { users } from "src/app/constants";
@Component({
    selector: 'add-remove-users',
    templateUrl: './add-remove-users.component.html',
    styleUrls: ['./add-remove-users.component.scss'],
})

export class AddRemoveUsers {
    
  validate = false;
  caseNumber;
  errorMessage;
  users = users;
  leftSelectedUser: any = [];
  selectedUser: any = [];

  onChangeEvent(event){
    if(this.caseNumber && this.caseNumber.length > 0){
      this.validate = true
    }
  }

  submit(){
    if(this.caseNumber == '123456'){
    }else{
      this.errorMessage = 'Case Number is not found'
    }
  }

  itemExist(user: any, list: any = this.leftSelectedUser) {
    return list.find((item) => item.code === user.code);
  }

  selectUser(user: any) {
    const exist = this.itemExist(user, this.selectedUser);
    if(!exist) {
      this.leftSelectedUser.push(user);
    }
  }

  move() {
    this.selectedUser = JSON.parse(JSON.stringify(this.leftSelectedUser));
    this.leftSelectedUser = [];
  }

  removeUser(event,i){
    if(event.checked){
      this.selectedUser[i].remove = event.checked;
    }else{
      delete this.selectedUser[i].remove;
    }
  }

  removeUsersInBulk() {
    this.selectedUser = this.selectedUser.filter(item => !item.remove);
  }

  clearAll () {
    this.selectedUser = [];
    this.leftSelectedUser = [];
  }

}
