import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
@Component({
    selector: 'manage-sensitive-case-access',
    templateUrl: './manage-sensitive-case-access.component.html',
    styleUrls: ['./manage-sensitive-case-access.component.scss'],
})

export class ManageSensitiveCaseAccess implements OnInit {
    
  validate = false;
  caseNumber;
  errorMessage;

  constructor(private router: Router){
    
  }
  ngOnInit() {

  }

  onChangeEvent(event){
    if(this.caseNumber && this.caseNumber.length > 0){
      this.validate = true
    }
  }

  submit(){
    if(this.caseNumber == '123456'){
      this.router.navigate(['home/manage-sensitive-case-access/add-remove-users']);
    }else{
      this.errorMessage = 'Case Number is not found'
    }
  }
}
