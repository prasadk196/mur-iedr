import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { auditorList } from './../../../constants';
@Component({
    selector: 'manage-audit-access',
    templateUrl: './manage-audit-access.component.html',
    styleUrls: ['./manage-audit-access.component.scss'],
})

export class ManageAuditAccess implements OnInit {
  validate = false;
  auditorList;
  selectedAuditor;
  filteredAuditorList;
  constructor(private router: Router) {
  }

  ngOnInit() {
    // if(!this.auditorList){
    //   this.auditorList = localStorage.setItem('auditorList',JSON.stringify(auditorList));
    // } 

    let getLocalAuditorList = localStorage.getItem('auditorList');
    this.auditorList = getLocalAuditorList && JSON.parse(getLocalAuditorList);

    let localStorageData = localStorage.getItem('selectedAuditor');
    this.selectedAuditor = localStorageData && JSON.parse(localStorageData);
    if(this.selectedAuditor){
      this.validate = true;
    }

    this.filteredAuditorList = this.auditorList.filter((item) => item.cases.length > 0);

  }
  selectAuditor(event){
    this.validate = true;
    this.selectedAuditor = event;
    console.log(event)
    localStorage.setItem('selectedAuditor', JSON.stringify(event));
  }
  submit(){
    console.log(this.selectedAuditor)
    if(this.selectedAuditor.id == 'new'){
      this.router.navigate(['home/manage-audit-access/new-auditor']);
    }else{
      this.router.navigate(['home/manage-audit-access/manage-audit-cases']);
    }
  }
}
