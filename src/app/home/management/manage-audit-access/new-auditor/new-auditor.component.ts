import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
@Component({
    selector: 'new-auditor',
    templateUrl: './new-auditor.component.html',
    styleUrls: ['./new-auditor.component.scss'],
})

export class NewAuditor implements OnInit {
  validate = false;
  auditorList;
  selectedAuditor;
  startDate = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
  endDate = new Date();
  selectCasesList : string[] = [];
  newCases;
  auditorValue;
  errorMessage;
  successMessage;
  newAuditor;
  constructor(private router: Router) {
  }
  ngOnInit() {
    let getLocalSelectedAuditor= localStorage.getItem('auditorList');
    this.auditorList = getLocalSelectedAuditor && JSON.parse(getLocalSelectedAuditor);
  }
 
  continue(){
    let cases = this.newCases ? this.newCases.split(',') : '';
    if(this.newAuditor && cases.length > 0){
      let obj = [{ id:this.newAuditor, name:this.newAuditor,cases: cases }];
      this.auditorList = [ ...this.auditorList , ...obj]
      localStorage.setItem('auditorList',JSON.stringify(this.auditorList));
      this.router.navigate(['home/manage-audit-access']);
    }else{
      alert('Case ID is required')
    }
  }

  reset(){
    this.newCases = '';
    this.newAuditor = '';
  }
 
}

