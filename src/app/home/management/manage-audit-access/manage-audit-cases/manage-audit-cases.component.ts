import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
@Component({
    selector: 'manage-audit-cases',
    templateUrl: './manage-audit-cases.component.html',
    styleUrls: ['./manage-audit-cases.component.scss'],
})

export class ManageAuditCases implements OnInit {
  validate = false;
  auditorList;
  selectedAuditor;
  startDate = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
  endDate = new Date();
  selectCasesList : string[] = [];
  newCases;
  auditorValue;
  errorMessage;
  successMessage;
  constructor(private router: Router) {
  }
  ngOnInit() {
    let localStorageData = localStorage.getItem('selectedAuditor');
    this.selectedAuditor = localStorageData && JSON.parse(localStorageData);


    
    let getLocalSelectedAuditor= localStorage.getItem('auditorList');
    this.auditorList = getLocalSelectedAuditor && JSON.parse(getLocalSelectedAuditor);
  }
 
  back(){
    this.router.navigate(['home/manage-audit-access']);
  }
  update(){

  }
  selectCases(event,item){
    if(event.checked[0]){
      this.selectCasesList.push(item)
    }else{
      this.selectCasesList.splice(item,1)
    }
  }

  selectAllCase() {
    // this.selectCasesList = this.selectedAuditor.cases;
  }
  
  caseAuditChanges(data){
    let cases = [];
    if(data == 'add'){
      let objIndex = this.auditorList.findIndex((obj => obj.name == this.auditorValue));
      if(objIndex !== -1){
        this.auditorList[objIndex].cases = [...this.auditorList[objIndex].cases, ...this.selectedAuditor.cases];
        this.auditorList[objIndex].cases = [ ...new Set(this.auditorList[objIndex].cases)];
        this.selectedAuditor = this.auditorList[objIndex];
        this.auditorValue = '';
      }else{
        this.errorMessage = this.auditorValue + ' is not an authorized auditor'
      }

    }else if(data == 'replace'){
      let objIndex = this.auditorList.findIndex((obj => obj.name == this.auditorValue));
      let currentAuditorIndex = this.auditorList.findIndex((item) => item.name == this.selectedAuditor.name);
      if(objIndex !== -1){
        this.auditorList[objIndex].cases = this.selectedAuditor.cases;
        this.auditorList[currentAuditorIndex].cases = [];
        let value = this.auditorList.filter((item) => item.name == this.auditorValue);
        this.selectedAuditor = value[0];
        this.auditorValue = ''
      }else{
        this.errorMessage = this.auditorValue + ' is not an authorized auditor'
      }
    }else if(data == 'revoke'){
       if(this.selectCasesList.length > 0){
        this.selectedAuditor.cases = this.selectedAuditor.cases.filter((item) => !this.selectCasesList.includes(item));
      }else{
        alert('No cases selected');
      }
    }else if(data == 'add-cases'){
      cases = this.newCases.split(',');
      this.selectedAuditor.cases = this.selectedAuditor.cases.concat(cases);
      this.successMessage = 'All entered cases have been assigned to '+this.selectedAuditor.name;
    }
    localStorage.setItem('selectedAuditor',JSON.stringify(this.selectedAuditor))
    this.auditorList.find((obj, index) => {
      if(obj.id == this.selectedAuditor.id){
        this.auditorList[index] = this.selectedAuditor;
      }
    });
    localStorage.setItem('auditorList',JSON.stringify(this.auditorList))
    this.newCases = '';
  }
}
