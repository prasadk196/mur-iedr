import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { users,eventTypes } from './../../../../constants';
@Component({
    selector: 'common-reports',
    templateUrl: './common-reports.component.html',
    styleUrls: ['./../common-reports.component.scss'],
})
 
export class CommonReports implements OnInit {
    id: any;
    title: any;
    users: any;
    eventTypes: any;
    reportsData;
    localStorageData = localStorage.getItem('city');
    

    constructor(private _Activatedroute:ActivatedRoute){

    }
    ngOnInit() {
        this.users = users;
        this.eventTypes = eventTypes;
        this.id = this._Activatedroute.snapshot.paramMap.get("id");
        this.title = this.id.replaceAll("-", " ");

        // this.reportsData.startDate = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
        // this.reportsData.endDate = new Date();
        let start = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
        let end =  new Date();
        
        let reportDetails = localStorage.getItem('report-details');

        if(reportDetails){
            this.reportsData = reportDetails && JSON.parse(reportDetails)
            this.reportsData.startDate = new Date(this.reportsData.startDate)
            this.reportsData.endDate = new Date(this.reportsData.endDate)
        }else{
            this.reportsData = {
                district: this.localStorageData && JSON.parse(this.localStorageData),
                startDate: start,
                endDate: end,
                userId: null,
                eventType: null
            }
        }
        
       
    }

    submit(){
        localStorage.setItem('report-details', JSON.stringify(this.reportsData));
    }

}
