import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import * as XLSX from 'xlsx';
import { reportsData } from '../../../../constants';

interface City {
    name: string,
    code: string
}
@Component({
    selector: 'reports-details',
    templateUrl: './reports-details.component.html',
    styleUrls: ['./reports-details.component.scss'],
})

export class ReportsDetailsComponent implements OnInit {

    id: any;
    title: any;
    allEventTypes;
    reportDetails;
    first = 0;
    rows = 10;
    fileName = 'ExcelSheet.xlsx';
    keyData: string[] = [];
    today = new Date();
    sortDetails;
    constructor(private _Activatedroute: ActivatedRoute) {

    }
    ngOnInit() {
        let details = localStorage.getItem('report-details');
        this.reportDetails = details && JSON.parse(details);


        let localStorageData = localStorage.getItem('city');
        this.reportDetails.district = localStorageData && JSON.parse(localStorageData);


        // this.id = this._Activatedroute.snapshot.paramMap.get("id");
        let url = window.location.pathname.split('/');
        this.id = url[url.length - 2]
        this.title = this.id.replaceAll("-", " ");

        this.allEventTypes = reportsData[this.id];
        var data: string[] = [];
        Object.keys(this.allEventTypes[0]).forEach(function (key) {
            data.push(key);
        });
        this.keyData = data;
        this.sortDetails = { field: this.keyData[0], order: 1 };

  
    }


    next() {
        this.first = this.first + this.rows;
    }

    prev() {
        this.first = this.first - this.rows;
    }

    exportexcel(): void {
        /* pass here the table id */
        let element = document.getElementById('excel-table');
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

        /* generate workbook and add the worksheet */
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        XLSX.writeFile(wb, this.fileName);

    }


    printTest() {
        window.print();
    }

    sortColumn(event){
        this.sortDetails = event;
    }

}
