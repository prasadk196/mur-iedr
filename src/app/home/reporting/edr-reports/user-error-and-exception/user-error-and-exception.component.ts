import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { users,eventTypes,cities } from '../../../../constants';
@Component({
    selector: 'user-error-and-exception',
    templateUrl: './user-error-and-exception.component.html',
    styleUrls: ['../common-reports.component.scss'],
})

export class UserErrorAndExceptionComponent implements OnInit {
    users: any;
    eventTypes: any;
    reportsData;
    localStorageData = localStorage.getItem('city');
    districtArray = cities;
    selectedCity;

    constructor(private _Activatedroute:ActivatedRoute){

    }
    ngOnInit() {
        this.users = users;
        this.eventTypes = eventTypes;
        let start = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
        let end =  new Date();
        
        let reportDetails = localStorage.getItem('report-details');
 
        if(reportDetails){
            this.reportsData = reportDetails && JSON.parse(reportDetails)
            this.reportsData.startDate = new Date(this.reportsData.startDate)
            this.reportsData.endDate = new Date(this.reportsData.endDate)
        }else{
            this.reportsData = {
                district: this.localStorageData && JSON.parse(this.localStorageData),
                startDate: start,
                endDate: end,
                userId: null,
                eventType: null
            }
        }
        let localStorageData = localStorage.getItem('city');
        this.selectedCity = localStorageData && JSON.parse(localStorageData);
       
    }

    submit(){
        localStorage.setItem('report-details', JSON.stringify(this.reportsData));
    }
    selectDistrict(event){
        localStorage.setItem('city',JSON.stringify(event));
    }
    selectValue(value, name){
        if(name == 'eventType'){
            this.reportsData.eventType = value;
        }else if(name == 'userId'){
            this.reportsData.userId = value;
        }
    }
}
