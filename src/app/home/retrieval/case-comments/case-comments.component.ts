import { Component, OnInit } from "@angular/core";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

interface City {
    name: string,
    code: string
}
@Component({
    selector: 'case-comments',
    templateUrl: './case-comments.component.html',
    styleUrls: ['./case-comments.component.scss'],
})

export class CaseCommentsComponent implements OnInit {

    
    public Editor = ClassicEditor;
    activeSection = 'null';
    myDateValue1: Date;
    myDateValue2: Date;
    comments: any;
    count: number;

    ngOnInit() {
        this.myDateValue1 = new Date();
        this.myDateValue2 = new Date();
        this.count = 0;
    }

    onDateChange(newDate: Date) {
        console.log(newDate);
    }

    receiveComment($event:any) {
        this.comments = $event;
        this.count = this.comments.length;
        console.log(this.comments.length);
    }


    recieveCount($event:any) {
        this.comments = $event;
        this.count = this.comments.length;
    }

    showComments(data){
        this.activeSection = data;
    }

    onClickSave(){
    //   saveDocument();
    }
}
