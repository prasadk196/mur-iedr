import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { SpinnerService } from "src/app/services/spinner.service";
import { cities } from '../../../constants';

@Component({
    selector: 'document-retrieval',
    templateUrl: './document-retrieval.component.html',
    styleUrls: ['./document-retrieval.component.scss'],
})

export class DocumentRetrievalComponent implements OnInit {
    districtArray = cities;
    selectedCategory: any = null;
    selectedOrphan: any = null;
    selectedCity;
    validate = false;
    searchCriteria;
    errorMessage;
    categories: any[] = [
        { name: 'CASE', key: 'case', detailTitle: 'Case Information' },
        { name: 'CIN/SSN', key: 'cin/ssn', detailTitle: 'Client Information  (Permanent Documents Only)' },
        { name: 'APPLICATION', key: 'application', detailTitle: 'Application Information' },
        { name: 'ORPHANS', key: 'orphans', detailTitle: 'Date Range for Orphans' }
    ];

    orphans: any[] = [
        { name: 'This Month', value: 'thisMonth' },
        { name: 'Previous Month', value: 'previousMonth' },
        { name: 'Older', value: 'older' }
    ]
    constructor(
        private router: Router,
        private spinnerService: SpinnerService
    ) {}
    ngOnInit() {
        this.selectedCategory = this.categories[0];
        this.selectedOrphan = this.orphans[0];
        let localStorageData = localStorage.getItem('city');
        this.selectedCity = localStorageData && JSON.parse(localStorageData);

        this.searchCriteria = {
            district: this.selectedCity,
            caseInfo: {
                caseNumber: null,
            },
            clientInfo: {
                cin: null,
                ssn: null
            },
            applicationInfo: {
                application: null
            },
            orphans: {
                value: this.selectedOrphan.value
            }
        }


    }
    selectDistrict(event){
        localStorage.setItem('city',JSON.stringify(event));
    }
    categoryChange(event){
        this.errorMessage = null;
        this.validate = false;
        if(this.selectedCategory.key == 'orphans'){
            this.validate = true;
        }
        this.searchCriteria = {
            district: this.selectedCity,
            caseInfo: {
                caseNumber: null,
            },
            clientInfo: {
                cin: null,
                ssn: null
            },
            applicationInfo: {
                application: null
            },
            orphans: {
                value: this.selectedOrphan.value
            }
        }
    }
    onChangeEvent(event) {

        this.validate = false;

        if ((this.searchCriteria.caseInfo.caseNumber && this.searchCriteria.caseInfo.caseNumber.length > 0) ||
            (this.searchCriteria.clientInfo.cin && this.searchCriteria.clientInfo.cin.length > 0) ||
            (this.searchCriteria.clientInfo.ssn && this.searchCriteria.clientInfo.ssn.length > 0) ||
            (this.searchCriteria.applicationInfo.application && this.searchCriteria.applicationInfo.application.length > 0)) {
            this.validate = true;
        }
    }

    submit() {
        if (this.selectedCategory.key == 'case') {
            if (this.searchCriteria.caseInfo.caseNumber) {
                this.errorMessage = null;
                localStorage.setItem('document-search-criteria', JSON.stringify(this.searchCriteria));
                this.router.navigate(['home/document-retrieval/view-documents']);
            } else {
                this.errorMessage = 'Case Number not found in your district (in WMS)';
            }
        } else if (this.selectedCategory.key == 'cin/ssn') {
            if (this.searchCriteria.clientInfo.cin || this.searchCriteria.clientInfo.ssn) {
                this.errorMessage = null;
                localStorage.setItem('document-search-criteria', JSON.stringify(this.searchCriteria));
                this.router.navigate(['home/document-retrieval/view-documents']);
            } else {
                this.errorMessage = 'CIN/SSN Number not found';
            }
        } else if (this.selectedCategory.key == 'application') {
            if (this.searchCriteria.applicationInfo.application) {
                this.errorMessage = null;
                localStorage.setItem('document-search-criteria', JSON.stringify(this.searchCriteria));
                this.router.navigate(['home/document-retrieval/view-documents']);
            } else {
                this.errorMessage = 'Application Number not found';
            }
        } else {
            localStorage.setItem('document-search-criteria', JSON.stringify(this.searchCriteria));
            this.router.navigate(['home/document-retrieval/view-documents']);
        }
    }
}
