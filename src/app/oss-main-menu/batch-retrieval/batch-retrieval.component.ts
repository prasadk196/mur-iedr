import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
@Component({
    selector: 'batch-retrieval',
    templateUrl: './batch-retrieval.component.html',
    styleUrls: ['./batch-retrieval.component.scss'],
})
 
export class BatchRetrievalComponent implements OnInit {
    retrievalForm: FormGroup;
    submitted = false;

    constructor(private _Activatedroute:ActivatedRoute,private formBuilder: FormBuilder){

    }
    ngOnInit() {
        this.retrievalForm = this.formBuilder.group({
            'case': ['', Validators.required],
            'scannedByType': ['', Validators.required],
            'indexerSelected': ['', Validators.required],
            'cin': ['', Validators.required],
            'scannedByUserId': ['', Validators.required],
            'batchName': ['', Validators.required],
            'app/reg': ['', Validators.required],
            'scannedByUserName': ['', Validators.required],
            'fromScannedDate': ['', Validators.required],
            'toScannedDate': ['', Validators.required],
        });
    }
    get f() { return this.retrievalForm.controls; }


    onSubmit(){
        this.submitted = true;

        if (this.retrievalForm.invalid) {
            return;
        }
    }


}
