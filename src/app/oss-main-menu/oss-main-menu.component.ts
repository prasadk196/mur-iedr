import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MessageService, PrimeNGConfig, MenuItem } from 'primeng/api';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';
import { ViewFileComponent } from '../common/view-file.component';
import { cities, auditorList } from '../constants';
import { SpinnerService } from '../services/spinner.service';

@Component({
  selector: 'app-oss-main-menu',
  templateUrl: './oss-main-menu.component.html',
  styleUrls: ['./oss-main-menu.component.scss'],
  providers: [MessageService]
})

export class OSSMainMenuComponent implements OnInit {
  districtArray = cities;

  mainMenu: MenuItem[];
  todayDate;
  buttonDisabled = true;
  selectedCity: any;
  @ViewChild('fileUpload') fileUpload: ElementRef;
  

  constructor(
    private primengConfig: PrimeNGConfig,
    public authService: AuthService,
    private apiService: ApiService,
    private modalService: BsModalService,
    private spinnerService: SpinnerService
  ) { }

  ngOnInit() {
    localStorage.removeItem('selectedAuditor');
    localStorage.setItem('auditorList',JSON.stringify(auditorList));

    this.todayDate = new Date();
    this.primengConfig.ripple = true;

    this.mainMenu = [{
      label: 'Retrieval',
      items: [{
        label: 'DOCUMENT RETRIEVAL',
        routerLink: 'document-retrieval',
      },
      {
        label: 'CASE COMMENTS',
        routerLink: 'case-comments',
      }
      ]
    },
    {
      label: 'Management',
      items: [{
        label: 'MANAGE SENSITIVE CASE ACCESS',
        routerLink: 'manage-sensitive-case-access',
      },
      {
        label: 'MANAGE AUDIT ACCESS',
        routerLink: 'manage-audit-access',
      }
      ]
    },
    {
      label: 'Reporting',
      items: [{
        label: 'I/EDR REPORTS',
        routerLink: 'edr-reports',
      }
      ]
    }
    ];

    let localStorageData = localStorage.getItem('city');
    this.selectedCity = localStorageData && JSON.parse(localStorageData);

  }

  selectDistrict(event){
    localStorage.setItem('city',JSON.stringify(event));
  }

  submit(){
    this.buttonDisabled = false;
  }

  citySelected() {
    return localStorage.getItem('city') ? true : false ;
  }

  goToLink(urlString: string) {
    const url = window.location.origin + '/oss-main-menu/' + urlString;
    // window.open(url, '_blank', 'toolbar=0,location=0,menubar=0,height=200,width=200');
    // window.open('http://www.google.com', '_blank', 'toolbar=0,location=0,menubar=0');
    // const newwindow: any = window.open("http://www.zeeshanakhter.com","_blank","toolbar=yes,scrollbars=yes, resizable=yes, top=500, left=500, width=400, height=400");
    // newwindow.moveTo(350,150);
  }

  async onFileChange(event) {
    console.log(event);
    if(event) {
      const [file] = event.target.files;
      let pdfSrc: any = '';
      let imgSrc: any = '';
      if(file) {
        if(file.type.includes('image')) {
          imgSrc = await this.toBase64(file);
        } else if(file.type.includes('pdf')) {
          pdfSrc = await this.toBase64(file);
        }
        const modalref: BsModalRef = this.modalService.show(ViewFileComponent, {
          class: 'modal-dialog-centered file-view-modal' ,
          initialState : { pdfSrc, imgSrc }
        });
        this.fileUpload.nativeElement.value = '';
        modalref.content.event.subscribe(res => {
          if(res && res.data) {
            // call api after api successfull
            this.apiService.scannedFileUpload(event).subscribe((res) => {
              this.spinnerService.setLoading(false);
              console.log(res);
            })
          }
        });
        modalref.onHide?.subscribe(() => {
          this.fileUpload.nativeElement.value = '';
        })
      }
    }
  }

  toBase64 = (file) => new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
  });

}
