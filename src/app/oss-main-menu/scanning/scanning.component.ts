import { Component, OnInit } from '@angular/core';
import {MessageService} from 'primeng/api';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'oss-scanning',
  templateUrl: './scanning.component.html',
  styleUrls: ['./scanning.component.scss'],
  providers: [MessageService]

})
export class OSSScanningComponent implements OnInit {
  activeState = [true, false, false];
  accordionList;
  showPdf = false;
  pdfSrc;
  constructor(
    private apiService: ApiService
  ) { }
  ngOnInit(): void {
    this.accordionList = [
      {header:'01 Permanent - HART , ERICA', docType: 'Citizenship Document', pages: 2, indexDate:'12/30/2012' },
      {header:'01 Permanent', docType: 'Citizenship Document', pages: 2, indexDate:'12/30/2012' },
      {header:'02 Applications / Recertification / Authorizations', docType: 'Citizenship Document', pages: 2, indexDate:'12/30/2012' }
    ];
  }
  toggle(index:number) {
    if(index == 1){
      this.activeState.forEach((element,index) => {
        this.activeState[index] = false;
      });
    }else{
      this.activeState.forEach((element,index) => {
        this.activeState[index] = true;
      });
    }
  }
  getPdfViewer(){
    this.showPdf = false;
    const documentId = 'A05BDA69-0000-CB13-A1C4-8426C1E8362E';
    this.apiService.getDocumentsById(documentId).subscribe((res) => {
      console.log(res);
      this.showPdf = true;
      this.pdfSrc = '../sample.pdf';
    }, ()=> {
      this.showPdf = false;
    })
  }



}
