import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'oss-reporting',
    templateUrl: './reporting.component.html',
    styleUrls: ['./reporting.component.scss'],
})
 
export class OSSReportingComponent implements OnInit {
    reportingList;
   
    constructor(private _Activatedroute:ActivatedRoute,private formBuilder: FormBuilder){

    }
    ngOnInit() {
       this.reportingList = [
         {path:'district-summary',name:'District Summary'},
         {path:'indexing-event-detail',name:'Indexing Event Detail'},
         {path:'scanning-event-detail',name:'Scanning Event Detail'},
         {path:'user-detail-summary',name:'User Detail Summary'}
        ]
    }


}
