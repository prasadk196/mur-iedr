import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { reportsData } from '../../../constants';
import * as XLSX from 'xlsx';

@Component({
    selector: 'oss-district-summary',
    templateUrl: './district-summary.component.html',
    styleUrls: ['./district-summary.component.scss'],
})
 
export class OSSDistrictSummaryComponent implements OnInit {
    districtSummaryForm: FormGroup;
    submitted = false;
    allEventTypes = reportsData['district-summary-report'];
    first = 0;
    rows = 10;
    fileName = 'ExcelSheet.xlsx';
    sortDetails;
    keyData: string[] = [];
    showTable = false;
    constructor(private _Activatedroute:ActivatedRoute,private formBuilder: FormBuilder){

    }
    ngOnInit() {
        this.districtSummaryForm = this.formBuilder.group({
            'beginDate': ['', Validators.required],
            'endDate': ['', Validators.required],
        });

        var data: string[] = [];
        Object.keys(this.allEventTypes[0]).forEach(function (key) {
            data.push(key);
        });
        this.keyData = data;
        this.sortDetails = { field: this.keyData[0], order: 1 };
    }
    get f() { return this.districtSummaryForm.controls; }


    onSubmit(){
        this.submitted = true;

        if (this.districtSummaryForm.invalid) {
            return;
        }

        this.showTable = true;
    }

    next() {
        this.first = this.first + this.rows;
    }
  
    prev() {
        this.first = this.first - this.rows;
    }
  
    exportexcel(): void {
        /* pass here the table id */
        let element = document.getElementById('excel-table');
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
  
        /* generate workbook and add the worksheet */
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  
        /* save to file */
        XLSX.writeFile(wb, this.fileName);
  
    }
  
  
    printTest() {
        window.print();
    }
  
    sortColumn(event){
        this.sortDetails = event;
    }


}
