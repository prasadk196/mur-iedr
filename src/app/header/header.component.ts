import { Component, Inject, OnInit } from '@angular/core';
import { OktaAuthStateService, OKTA_AUTH } from '@okta/okta-angular';
import { OktaAuth } from '@okta/okta-auth-js';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isOSS = false;
  isAuthenticated: boolean = false;

  constructor(
    @Inject(OKTA_AUTH) private oktaAuth: OktaAuth,
    public authStateService: OktaAuthStateService
  ) {
  }

  items: MenuItem[];

  async ngOnInit() {
    this.items = [
      {
        label: 'Profile',
        icon: 'pi pi-fw pi-pencil',
      },
      {
        label: 'Settings',
        icon: 'pi pi-fw pi-pencil',
      },
      {
        label: 'Logout',
        icon: 'pi pi-fw pi-pencil',
      }
    ];
    this.isOSS = window.location.pathname.includes('oss-main-menu');
  }

  closeWindow() {
    window.close();
  }

  async login() {
    await this.oktaAuth.signInWithRedirect();
  }

  async logout() {
    await this.oktaAuth.signOut();
  }

}
