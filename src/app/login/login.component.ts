import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { cities } from '../constants';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    cities = cities;

    oktaSignin: any

    constructor(
        private formBuilder: FormBuilder, private authService: AuthService,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
        // this.oktaSignin.renderEl({
        //     el: '#okta-sign-in-widget'
        // }, (response) => {
        //     if(response.status === 'SUCCESS') {
        //         this.oktaAuthService.signInWithRedirect();
        //     }
        // }, (error) => {
        //     console.log(error);
        // })
    }

    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        if (this.loginForm.invalid) {
            return;
        }
        localStorage.setItem('city', JSON.stringify(cities[0]));
        this.router.navigate(['home']);
    }

    ngOnDestroy(): void {
        if(this.oktaSignin) {
            this.oktaSignin.remove();
        }
    }
}
