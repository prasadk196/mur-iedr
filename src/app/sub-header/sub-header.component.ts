import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OktaAuthStateService, OKTA_AUTH } from '@okta/okta-angular';
import { OktaAuth } from '@okta/okta-auth-js';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.scss'],
})
export class SubHeaderComponent implements OnInit {

  isAuthenticated: boolean = false;
  user: any = null;
  todayDate = new Date();

  constructor(
    public authService:AuthService,
    @Inject(OKTA_AUTH) private oktaAuth: OktaAuth,
    public authStateService: OktaAuthStateService
  ) {}

  async ngOnInit() {
    this.user = await this.oktaAuth.getUser();
  }

  getSelectedCity() {
    var city = localStorage.getItem('city');
    return city && JSON.parse(city).name;
  }

  checkIsOSSApp() {
    return (window.location.pathname.includes('oss-main-menu') || window.location.pathname.includes('landing'));
  }

}
