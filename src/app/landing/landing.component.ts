import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth.service";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})

export class LandingComponent implements OnInit {

  constructor(
    public authService: AuthService
  ) {

  }
  ngOnInit(): void {
    // throw new Error("Method not implemented.");
  }

  myFunction() {
    window.open('https://www.geeksforgeeks.org/','geeks', 'toolbars=0,width=300,height=300,left=200, top=200,scrollbars=1,resizable=1');
  }

  openModal() {
    window.open(window.location.origin + '/oss-main-menu');
  }
}